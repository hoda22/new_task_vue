import Vue from 'vue'
import router from './router'
import App from './App.vue'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'  

Vue.config.productionTip = false
Vue.prototype.$host = "http://localhost/task/api/v1/"
new Vue({
  render: h => h(App),
  router
}).$mount('#app')
