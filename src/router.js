import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from './components/Home'
import Login from './components/Login'
import Note from './components/Note'
import NoteEdit from './components/NoteEdit'
import NoteCreate from './components/NoteCreate'

Vue.use(VueRouter)

const router = new VueRouter({
    routes :[
        {
            path: '/',
            name: 'home',
            component: Home,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/note/:id', 
            name: 'note', 
            component: Note,
            meta: {
                requiresAuth: true
            }
        }, 
        {
            path: '/note/:id/edit', 
            name:'noteEdit' ,
            component: NoteEdit ,
            meta: {
                requiresAuth: true
            }   
        }, 
        {
            path: '/create',
            name:'noteCreate',
            component: NoteCreate ,
            meta: {
                requiresAuth: true
            }
        }
    ]
});

router.beforeEach((to, from, next) => {
    if( to.matched.some( record => record.meta.requiresAuth )){ 
        if(localStorage.getItem('token') == null){ 
            next({
                path:'/login'
            }) 
        }else{
            next();
        }
    }else{ 
        next();
    }
}) 
export default router